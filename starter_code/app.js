require("dotenv").config();

const express = require("express");
const pug = require("pug");
// require spotify-web-api-node package here:
const SpotifyWebApi = require("spotify-web-api-node");
const bodyParser = require("body-parser");

const app = express();

app.set("view engine", "pug");
app.set("views", `${__dirname}/views`);
//use static
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));


// setting the spotify-api goes here:
const spotifyApi = new SpotifyWebApi({
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET
});

spotifyApi
  .clientCredentialsGrant()
  .then(data => {
    console.log(`Spotify API access done ${{data}} 💀`);
    spotifyApi.setAccessToken(data.body["access_token"]);
  })
  .catch(error => {
    console.log("Something went wrong when retrieving an access token", error);
  });

// the routes go here:
app.get("/", (req, res) => {
  res.render("home");
});

app.post("/artist", (req, res) => {
    let artist = req.body.artist
  console.log(`estas en artist con los datos ${artist}`);
  spotifyApi
    .searchArtists(`${artist}`)
    .then(data => {
       const {items} = data.body.artists
      //console.log("The received data from the API: ", items);
      items.forEach( item => {
        if(item.images.length !== 0){
          item.images = item.images[1];
        }else{
          item.images= { url:"https://www.nocowboys.co.nz/images/v3/no-image-available.png"}
        }       })
     res.render("artist", {items})

      // ----> 'HERE WHAT WE WANT TO DO AFTER RECEIVING THE DATA FROM THE API'
    })
    .catch(err => {
      console.log("The error while searching artists occurred: ", err);
    });
});


app.get("/albums/:artistId", (req, res) => {
  let { artistId } = req.params;
  // res.send(artistId);
  spotifyApi
    .getArtistAlbums(artistId)
    .then(data => {
      let { items } = data.body;
      items.artistName = items[0].artists[0].name;
      items.forEach(item => {
        if (item.images.length !== 0) {
          item.images = item.images[1];
        } else {
          item.images = {
            url:
            "https://www.nocowboys.co.nz/images/v3/no-image-available.png"
          };
        }
      });
      // res.send(items);
      res.render("albums", { items });
    })
    .catch(err => console.log(err));
});


app.get("/tracks/:trackId", (req, res) => {
  let { trackId } = req.params;
  spotifyApi
    .getAlbumTracks(trackId)
    .then(data => {
      let { items } = data.body;
      res.render("tracks", { items });
    })
    .catch(err => console.log(err));
});


app.listen(3000, () =>
  console.log("My Spotify project running on port 3000 🎧 🥁 🎸 🔊")
);
